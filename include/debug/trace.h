#pragma once

/*
 * C++ tracing facility library.
 *
 * Copyright © 2001-2022 Giampiero Gabbiani (giampiero@gabbiani.org)
 *
 * This file is part of the 'Cpp Trace library' (CPPTRACE) project.
 *
 * CPPTRACE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CPPTRACE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CPPTRACE.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <stack>

namespace debug {

extern std::string repeat(std::string str, const size_t n);

inline std::string operator * (std::string str, size_t n) {
  return repeat(std::move(str), n);
}

/**
 * Simple trace facility class based on RAII.
 * TODO: implement a runtime mechanism for trace disable
 */
class Trace {
  using String  = std::string;
  using Stack   = std::stack<String>;

public:
  explicit Trace(const String& name);
  explicit Trace(const char* name) : Trace(String(name)) {}
  ~Trace();

  static const String &prefix() {return _prefix;}

private:
  void right() const;
  void left() const;

  String open() const;
  String close() const;

  const String        _name;

  static String       _prefix;
  static String       _level;
  static Stack        _stack;
  static int          _n;

  static const String _color[];
  static const String _reset;
  static const String _bar;
};

namespace details {

template<typename ...Args>
std::ostream& arguments(Args&&... args) {
  std::string delim = "";
  // [fold expression(since C++17)](https://en.cppreference.com/w/cpp/language/fold)
  ((std::cout << Trace::prefix() << "  "), ... , (std::cout << delim << args, delim = " "));
  return std::cout << std::endl;
}

} // namespace details

}  // namespace debug

#if !defined NDEBUG && !defined NTRACE
/** new trace object function named */
#define TR_FUNC debug::Trace __trace__(__PRETTY_FUNCTION__)
/** new trace named by parameter */
#define TR_NEW(name) debug::Trace __trace__(name)
/** trace a multi-arg message */
#define TR_MSG(...) debug::details::arguments(__VA_ARGS__)
#else
#define TR_FUNC
#define TR_NEW(name)
#define TR_MSG(...)
#endif
