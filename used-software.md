# Used software list

The following software is used by cpp-trace and included via CMake FetchContent.

## Software list and related licenses

- [termcolor](https://github.com/ikalnytskyi/termcolor) [^1]

[^1]: [termcolor license](https://github.com/ikalnytskyi/termcolor/blob/master/LICENSE)
